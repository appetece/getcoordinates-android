package com.socialprotools.getcoordenates;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class GetCoordMainActivity extends AppCompatActivity {

    TextView txtLatitude;
    TextView txtLongitude;
    Context contextMain;
    GPSTracker gpsTracker;

    public static final int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_coord_main);

        contextMain = getApplicationContext();

        txtLatitude = (TextView)findViewById(R.id.txtLatitude);
        txtLongitude = (TextView)findViewById(R.id.txtLongitude);

        if (ContextCompat.checkSelfPermission(contextMain,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);

        }
        gpsTracker = new GPSTracker(contextMain);

        showCoord();

        setTimer(6000);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void setTimer(final int mlseconds){
        final Handler h = new Handler();
        h.postDelayed(new Runnable() {
            private long time = 0;

            @Override
            public void run() {

                // do stuff then
                // can call h again after work!
                time += 1000;
                //Log.d("TimerExample", "Going for... " + time);
                h.postDelayed(this, 1000);

                if (time >= mlseconds) {
                    //sendJsonToServer();
                    //setVariablesControl(true);
                    Log.d("Debug", "Timer");
                    showCoord();
                    time = 0;
                }
            }
        }, 1000); // 1 second delay (takes millis)
    }

    private void showCoord(){
        txtLatitude.setText("Latitude:" + gpsTracker.getLatitude());
        txtLongitude.setText("Longitude:" + gpsTracker.getLongitude());
    }
}
